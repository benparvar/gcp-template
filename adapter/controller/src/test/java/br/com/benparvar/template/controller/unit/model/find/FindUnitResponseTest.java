package br.com.benparvar.template.controller.unit.model.find;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FindUnitResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        FindUnitResponse unit1 = new FindUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        FindUnitResponse unit2 = new FindUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        FindUnitResponse unit1 = new FindUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        FindUnitResponse unit2 = new FindUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");
        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        FindUnitResponse unit = new FindUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        FindUnitResponse unit = new FindUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("FindUnitResponse{code='LT', name='LITRE'}", unit.toString());
    }
}