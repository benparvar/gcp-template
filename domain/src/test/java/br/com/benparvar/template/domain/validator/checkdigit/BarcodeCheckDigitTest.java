package br.com.benparvar.template.domain.validator.checkdigit;

import br.com.benparvar.template.domain.exception.CheckDigitException;
import br.com.benparvar.template.domain.validator.checkdigit.BarcodeCheckDigit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Ean 13 check digit test.
 */
class BarcodeCheckDigitTest {
    /**
     * The Check digit.
     */
    BarcodeCheckDigit checkDigit;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        checkDigit = new BarcodeCheckDigit();
    }

    /**
     * Execute calculate with null code will fail.
     */
    @Test
    public void executeCalculateWithNullCodeWillFail() {
        assertThrows(CheckDigitException.class, () -> {

            String code = null;
            try {
                checkDigit.calculate(code);
            } catch (CheckDigitException e) {
                assertEquals("code cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an CheckDigitException");
        });
    }

    /**
     * Execute calculate with empty code will fail.
     */
    @Test
    public void executeCalculateWithEmptyCodeWillFail() {
        assertThrows(CheckDigitException.class, () -> {

            String code = "";
            try {
                checkDigit.calculate(code);
            } catch (CheckDigitException e) {
                assertEquals("code cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an CheckDigitException");
        });
    }

    /**
     * Execute calculate with non numeric code will fail.
     */
    @Test
    public void executeCalculateWithNonNumericCodeWillFail() {
        assertThrows(CheckDigitException.class, () -> {

            String code = "a1234567";
            try {
                checkDigit.calculate(code);
            } catch (CheckDigitException e) {
                assertEquals("invalid Character[1] = 'a'", e.getMessage());
                throw e;
            }

            fail("should throw an CheckDigitException");
        });
    }

    /**
     * Execute calculate with numeric ean 8 code will success.
     */
    @Test
    public void executeCalculateWithNumericEan8CodeWillSuccess() {
        String code = "12345678"; // 123456784

        String checkDigit = this.checkDigit.calculate(code);

        assertNotNull(checkDigit);
        assertEquals("4", checkDigit);
    }

    /**
     * Execute calculate with numeric ean 13 code will success.
     */
    @Test
    public void executeCalculateWithNumericEan13CodeWillSuccess() {
        String code = "123456789012"; // 1234567890128

        String checkDigit = this.checkDigit.calculate(code);

        assertNotNull(checkDigit);
        assertEquals("8", checkDigit);
    }

    /**
     * Execute calculate with numeric upca code will success.
     */
    @Test
    public void executeCalculateWithNumericUPCACodeWillSuccess() {
        String code = "12345678901"; // 123456789012

        String checkDigit = this.checkDigit.calculate(code);

        assertNotNull(checkDigit);
        assertEquals("2", checkDigit);
    }

    /**
     * Execute is valid with null code code will return false.
     */
    @Test
    public void executeIsValidWithNullCodeCodeWillReturnFalse() {
        String code = null;

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(false, result);
    }

    /**
     * Execute is valid with empty code code will return false.
     */
    @Test
    public void executeIsValidWithEmptyCodeCodeWillReturnFalse() {
        String code = "";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(false, result);
    }

    /**
     * Execute is valid with numeric ean 8 invalid code will return false.
     */
    @Test
    public void executeIsValidWithNumericEan8InvalidCodeWillReturnFalse() {
        String code = "123456789";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(false, result);
    }

    /**
     * Execute is valid with numeric ean 8 valid code will return true.
     */
    @Test
    public void executeIsValidWithNumericEan8ValidCodeWillReturnTrue() {
        String code = "123456784";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(true, result);
    }

    /**
     * Execute is valid with numeric ean 13 invalid code will return false.
     */
    @Test
    public void executeIsValidWithNumericEan13InvalidCodeWillReturnFalse() {
        String code = "1234567890121";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(false, result);
    }

    /**
     * Execute is valid with numeric ean 13 valid code will return true.
     */
    @Test
    public void executeIsValidWithNumericEan13ValidCodeWillReturnTrue() {
        String code = "1234567890128";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(true, result);
    }

    /**
     * Execute is valid with numeric upca invalid code will return false.
     */
    @Test
    public void executeIsValidWithNumericUPCAInvalidCodeWillReturnFalse() {
        String code = "123456789019";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(false, result);
    }

    @Test
    public void executeIsValidWithNonNumericCodeWillReturnFalse() {
        String code = "12345678e9012";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(false, result);
    }

    /**
     * Execute is valid with numeric upca valid code will return true.
     */
    @Test
    public void executeIsValidWithNumericUPCAValidCodeWillReturnTrue() {
        String code = "123456789012";

        boolean result = this.checkDigit.isValid(code);

        assertNotNull(result);
        assertEquals(true, result);
    }

    /**
     * Execute get modulus will return ten.
     */
    @Test
    public void executeGetModulusWillReturnTen() {
        int result = checkDigit.getModulus();

        assertNotNull(result);
        assertEquals(10, result);
    }

    /**
     * Execute sum digits with valid number.
     */
    @Test
    public void executeSumDigitsWithValidNumber() {
        String code = "1236";

        int result = this.checkDigit.sumDigits(Integer.valueOf(code));

        assertNotNull(result);
        assertEquals(12, result);
    }
}