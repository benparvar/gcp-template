package br.com.benparvar.template.controller.unit.model.find;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find unit mapper test.
 */
class FindUnitMapperTest {
    private FindUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = FindUnitMapper.INSTANCE;
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Unit entity = null;
        FindUnitResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Unit entity = Unit.builder().code("MT").name("Meter").build();

        FindUnitResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute entity to response with a null entity list will return a null list.
     */
    @Test
    public void executeEntityToResponseWithANullEntityListWillReturnANullList() {
        List<Unit> entities = null;
        List<FindUnitResponse> responses = mapper.entityToResponse(entities);

        assertNull(responses, "must be null");
    }

    /**
     * Execute entity to response with a non empty entity list will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonEmptyEntityListWillReturnWithSuccess() {
        Unit entity = Unit.builder().code("MT").name("Meter").build();

        List<FindUnitResponse> responses = mapper.entityToResponse(Arrays.asList(entity));

        assertNotNull(responses, "do not must be null");
        assertFalse(responses.isEmpty());
        assertEquals(1, responses.size());

        FindUnitResponse response = responses.get(0);

        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }
}