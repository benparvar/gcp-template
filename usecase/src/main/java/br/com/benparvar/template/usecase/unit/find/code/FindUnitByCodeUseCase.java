package br.com.benparvar.template.usecase.unit.find.code;

import br.com.benparvar.template.domain.unit.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Find unit by code use case.
 */
public class FindUnitByCodeUseCase {
    private final Logger log = LoggerFactory.getLogger(FindUnitByCodeUseCase.class);
    private final FindUnitByCode findUnitByCodeGateway;

    /**
     * Instantiates a new Find unit by code use case.
     *
     * @param findUnitByCodeGateway the find unit by code gateway
     */
    public FindUnitByCodeUseCase(FindUnitByCode findUnitByCodeGateway) {
        this.findUnitByCodeGateway = findUnitByCodeGateway;
    }

    /**
     * Execute unit.
     *
     * @param code the code
     * @return the unit
     */
    public Unit execute(String code) {
        log.info("execute: {}", code);

        return findUnitByCodeGateway.findByCode(code);
    }
}
