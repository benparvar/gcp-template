package br.com.benparvar.template.controller.unit.model.delete;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Delete unit request test.
 */
class DeleteUnitRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        DeleteUnitRequest unit1 = DeleteUnitRequest.newBuilder().code("LT").name("LITRE").build();
        DeleteUnitRequest unit2 = DeleteUnitRequest.newBuilder().code("LT").name("LITRE").build();

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        DeleteUnitRequest unit1 = DeleteUnitRequest.newBuilder().code("LT").name("LITRE").build();
        DeleteUnitRequest unit2 = DeleteUnitRequest.newBuilder().code("LT").name("LITRE").build();

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        DeleteUnitRequest unit = DeleteUnitRequest.newBuilder().code("LT").name("LITRE").build();

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        DeleteUnitRequest unit = DeleteUnitRequest.newBuilder().code("LT").name("LITRE").build();

        assertNotNull(unit);
        assertEquals("DeleteUnitRequest{code='LT', name='LITRE'}", unit.toString());
    }
}