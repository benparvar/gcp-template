package br.com.benparvar.template.usecase.unit.update;

import br.com.benparvar.template.domain.unit.Unit;

/**
 * The interface Update unit.
 */
public interface UpdateUnit {
    /**
     * Update unit.
     *
     * @param entity the entity
     * @return the unit
     */
    Unit update(Unit entity);
}
