package br.com.benparvar.template.usecase.unit.delete;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate unit before delete use case test.
 */
class ValidateUnitBeforeDeleteUseCaseTest {
    private ValidateUnitBeforeDeleteUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateUnitBeforeDeleteUseCase();
    }

    /**
     * Execute with a null unit will fail.
     */
    @Test
    public void executeWithANullUnitWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = null;

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty code and name will fail.
     */
    @Test
    public void executeWithEmptyCodeAndNameWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = Unit.builder().build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty code will fail.
     */
    @Test
    public void executeWithEmptyCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = Unit.builder().name("this is a name").build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty name will success.
     */
    @Test
    public void executeWithEmptyNameWillSuccess() {
        Unit entity = Unit.builder().code("this is a code").build();

        boolean result = uc.execute(entity);
        assertTrue(result, "must be a valid code");
    }

    /**
     * Execute with a valid unit will success.
     */
    @Test
    public void executeWithAValidUnitWillSuccess() {
        Unit entity = Unit.builder().code("this is a code").name("this is a name").build();

        boolean result = uc.execute(entity);

        assertTrue(result, "must be a valid unit");
    }
}