package br.com.benparvar.template.domain.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * The type Check digit exception test.
 */
class CheckDigitExceptionTest {


    /**
     * Check exception integrity without message.
     */
    @Test
    public void checkExceptionIntegrityWithoutMessage() {
        assertThrows(CheckDigitException.class, () -> {
            try {
                throw new CheckDigitException();
            } catch (CheckDigitException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity with message.
     */
    @Test
    public void checkExceptionIntegrityWithMessage() {
        assertThrows(CheckDigitException.class, () -> {
            try {
                throw new CheckDigitException("royksopp exception");
            } catch (CheckDigitException e) {
                assertEquals("royksopp exception", e.getMessage());
                throw e;
            }
        });
    }
}