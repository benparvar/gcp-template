package br.com.benparvar.template.repository.unit;

import br.com.benparvar.template.repository.unit.model.UnitModel;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QuerySnapshot;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * The interface Unit database.
 */
public class UnitFirestoreRepository implements UnitRepository {
    private final Firestore firestore;
    private static final String COLLECTION = "unit";
    private static final String CODE = "code";

    public UnitFirestoreRepository(Firestore firestore) {
        this.firestore = firestore;
    }

    @Override
    public UnitModel findByCode(String code) {
        UnitModel model = null;
        Query query = firestore.collection(COLLECTION).whereEqualTo(CODE, code);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();

        try {
            if (querySnapshot.get().getDocuments().isEmpty())
                return model;

            model = querySnapshot.get().getDocuments().get(0).toObject(UnitModel.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return model;
    }

    @Override
    public UnitModel save(UnitModel model) {
        if (model.getId() == null)
            model.setId(UUID.randomUUID().toString());

        firestore.collection(COLLECTION).document(model.getId()).set(model);

        return model;
    }

    @Override
    public List<UnitModel> findAll() {
        return null;
    }

    @Override
    public void delete(UnitModel model) {
        firestore.collection(COLLECTION).document(model.getId()).delete();
    }
}
