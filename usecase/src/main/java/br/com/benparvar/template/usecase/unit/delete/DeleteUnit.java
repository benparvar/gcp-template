package br.com.benparvar.template.usecase.unit.delete;

import br.com.benparvar.template.domain.unit.Unit;

/**
 * The interface Delete unit.
 */
public interface DeleteUnit {

    /**
     * Delete.
     *
     * @param entity the entity
     */
    void delete(Unit entity);
}
