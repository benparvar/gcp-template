package br.com.benparvar.template.spring.controller.unit;

import br.com.benparvar.template.controller.unit.UnitController;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitRequest;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitResponse;
import br.com.benparvar.template.controller.unit.model.find.FindUnitResponse;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitRequest;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitResponse;
import br.com.benparvar.template.domain.exception.AlreadyExistException;
import br.com.benparvar.template.domain.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

/**
 * The type Spring unit controller.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/units", produces = MediaType.APPLICATION_JSON_VALUE)
public class SpringUnitController {
    private final Logger log = LoggerFactory.getLogger(SpringUnitController.class);
    @Autowired
    private final UnitController controller;

    /**
     * Instantiates a new Spring unit controller.
     *
     * @param controller the controller
     */
    public SpringUnitController(UnitController controller) {
        this.controller = controller;
    }

    /**
     * Create create unit response.
     *
     * @param request the request
     * @return the create unit response
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CreateUnitResponse create(@RequestBody CreateUnitRequest request) {
        log.info("create: {}", request);

        return controller.create(request);
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<FindUnitResponse> findAll() {
        log.info("findAll:");

        return controller.findAll();
    }

    /**
     * Find by code find unit response.
     *
     * @param code the code
     * @return the find unit response
     */
    @GetMapping(value = "/{code}/code", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public FindUnitResponse findByCode(@PathVariable String code) {
        log.info("findByCode: {}", code);

        return controller.findByCode(code);
    }

    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UpdateUnitResponse update(@RequestBody UpdateUnitRequest request) {
        log.info("update: {}", request);

        return controller.update(request);
    }

    @DeleteMapping(value = "/{code}/code")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String code) {
        log.info("delete: {}", code);

        controller.delete(code);
    }

    /**
     * Handle not found exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Sorry...", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }


    /**
     * Handle already exist exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({AlreadyExistException.class})
    public ResponseEntity<Object> handleAlreadyExistException(AlreadyExistException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Already exist", new HttpHeaders(), HttpStatus.CONFLICT);
    }

    /**
     * Handle illegal argument exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle http message not readable exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex,
                                                                        WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
