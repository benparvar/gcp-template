package br.com.benparvar.template.domain.validator.checkdigit;

import br.com.benparvar.template.domain.exception.CheckDigitException;

/**
 * The type Modulus check digit.
 *
 * Abstract <b>Modulus</b> Check digit calculation/validation.
 * <p>
 * Provides a <i>base</i> class for building <i>modulus</i> Check
 * Digit routines.
 * <p>
 * This implementation only handles <i>single-digit numeric</i> codes, such as
 * <b>EAN-13</b>. For <i>alphanumeric</i> codes such as <b>EAN-128</b> you
 * will need to implement/override the <code>toInt()</code> and
 * <code>toChar()</code> methods.
 * <p>
 *
 */
public abstract class ModulusCheckDigit implements CheckDigit {
    private final int modulus;

    /**
     * Instantiates a new Modulus check digit.
     *
     * @param modulus the modulus
     */
    public ModulusCheckDigit(int modulus) {
        this.modulus = modulus;
    }

    /**
     * Gets modulus.
     *
     * @return the modulus
     */
    public int getModulus() {
        return modulus;
    }

    @Override
    public String calculate(String code) throws CheckDigitException {
        if (code == null || code.length() == 0) {
            throw new CheckDigitException("code cannot be null or empty");
        }
        int modulusResult = calculateModulus(code, false);
        int charValue = (modulus - modulusResult) % modulus;
        return toCheckDigit(charValue);
    }

    @Override
    public boolean isValid(String code) {
        if (code == null || code.length() == 0) {
            return false;
        }
        try {
            int modulusResult = calculateModulus(code, true);
            return (modulusResult == 0);
        } catch (CheckDigitException  ex) {
            return false;
        }
    }

    /**
     * Calculate modulus int.
     *
     * @param code               the code
     * @param includesCheckDigit the includes check digit
     * @return the int
     * @throws CheckDigitException the check digit exception
     */
    protected int calculateModulus(String code, boolean includesCheckDigit) throws CheckDigitException {
        int total = 0;
        for (int i = 0; i < code.length(); i++) {
            int lth = code.length() + (includesCheckDigit ? 0 : 1);
            int leftPos  = i + 1;
            int rightPos = lth - i;
            int charValue = toInt(code.charAt(i), leftPos, rightPos);
            total += weightedValue(charValue, leftPos, rightPos);
        }
        if (total == 0) {
            throw new CheckDigitException("invalid code, sum is zero");
        }
        return total % modulus;
    }

    /**
     * Weighted value int.
     *
     * @param charValue the char value
     * @param leftPos   the left pos
     * @param rightPos  the right pos
     * @return the int
     * @throws CheckDigitException the check digit exception
     */
    protected abstract int weightedValue(int charValue, int leftPos, int rightPos)
            throws CheckDigitException;

    /**
     * To int int.
     *
     * @param character the character
     * @param leftPos   the left pos
     * @param rightPos  the right pos
     * @return the int
     * @throws CheckDigitException the check digit exception
     */
    protected int toInt(char character, int leftPos, int rightPos)
            throws CheckDigitException {
        if (Character.isDigit(character)) {
            return Character.getNumericValue(character);
        }
        throw new CheckDigitException("invalid Character[" +
                leftPos + "] = '" + character + "'");
    }

    /**
     * To check digit string.
     *
     * @param charValue the char value
     * @return the string
     * @throws CheckDigitException the check digit exception
     */
    protected String toCheckDigit(int charValue)
            throws CheckDigitException {
        if (charValue >= 0 && charValue <= 9) { // CHECKSTYLE IGNORE MagicNumber
            return Integer.toString(charValue);
        }
        throw new CheckDigitException("invalid Check Digit Value =" +
                + charValue);
    }

    /**
     * Sum digits int.
     *
     * @param number the number
     * @return the int
     */
    public static int sumDigits(int number) {
        int total = 0;
        int todo = number;
        while (todo > 0) {
            total += todo % 10; // CHECKSTYLE IGNORE MagicNumber
            todo  = todo / 10; // CHECKSTYLE IGNORE MagicNumber
        }
        return total;
    }
}
