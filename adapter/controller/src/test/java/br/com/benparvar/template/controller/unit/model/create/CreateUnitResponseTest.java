package br.com.benparvar.template.controller.unit.model.create;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Create unit response test.
 */
class CreateUnitResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateUnitResponse unit1 = new CreateUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateUnitResponse unit2 = new CreateUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateUnitResponse unit1 = new CreateUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateUnitResponse unit2 = new CreateUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");
        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateUnitResponse unit = new CreateUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateUnitResponse unit = new CreateUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("CreateUnitResponse{code='LT', name='LITRE'}", unit.toString());
    }
}