package br.com.benparvar.template.spring.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Configuration
public class FireStoreConfig {
    @Value("${gcp.firestore.credentials.file}")
    private Resource resourceFile;

    @Bean
    public Firestore getFirestore() throws IOException {
        GoogleCredentials credentials = GoogleCredentials.fromStream(resourceFile.getInputStream());
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .setDatabaseUrl("${gcp.firestore.database.url}")
                .build();

        FirebaseApp.initializeApp(options);

        return FirestoreClient.getFirestore();
    }
}
