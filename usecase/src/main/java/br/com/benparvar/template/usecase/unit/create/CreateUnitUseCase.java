package br.com.benparvar.template.usecase.unit.create;

import br.com.benparvar.template.domain.unit.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Create unit use case.
 */
public class CreateUnitUseCase {
    private final Logger log = LoggerFactory.getLogger(CreateUnitUseCase.class);
    private final ValidateUnitBeforeCreateUseCase validateUnitBeforeCreateUseCase;
    private final SaveUnit saveUnitGateway;

    /**
     * Instantiates a new Create unit use case.
     *
     * @param validateUnitBeforeCreateUseCase the validate unit before create use case
     * @param saveUnitGateway                 the save unit gateway
     */
    public CreateUnitUseCase(ValidateUnitBeforeCreateUseCase validateUnitBeforeCreateUseCase, SaveUnit saveUnitGateway) {
        this.validateUnitBeforeCreateUseCase = validateUnitBeforeCreateUseCase;
        this.saveUnitGateway = saveUnitGateway;
    }

    /**
     * Execute unit.
     *
     * @param entity the entity
     * @return the unit
     */
    public Unit execute(Unit entity) {
        log.info("execute: {}", entity);
        validateUnitBeforeCreateUseCase.execute(entity);

        return saveUnitGateway.save(entity);
    }
}
