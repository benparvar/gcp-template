package br.com.benparvar.template.domain.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * The type Not found exception test.
 */
class NotFoundExceptionTest {

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new NotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

}