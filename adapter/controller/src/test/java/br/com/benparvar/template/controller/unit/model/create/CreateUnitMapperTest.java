package br.com.benparvar.template.controller.unit.model.create;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Create unit mapper test.
 */
class CreateUnitMapperTest {

    private CreateUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = CreateUnitMapper.INSTANCE;
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Unit entity = null;
        CreateUnitResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Unit entity = Unit.builder().code("MT").name("Meter").build();

        CreateUnitResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        CreateUnitRequest request = null;
        Unit unit = mapper.requestToEntity(request);

        assertNull(unit, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        CreateUnitRequest request = new CreateUnitRequest();
        request.setCode("MT");
        request.setName("Meter");

        Unit unit = mapper.requestToEntity(request);

        assertNotNull(unit, "do not must be null");
        assertEquals("MT", unit.getCode());
        assertEquals("Meter", unit.getName());
    }
}