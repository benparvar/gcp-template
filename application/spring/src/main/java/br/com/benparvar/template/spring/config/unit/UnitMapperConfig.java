package br.com.benparvar.template.spring.config.unit;

import br.com.benparvar.template.controller.unit.model.create.CreateUnitMapper;
import br.com.benparvar.template.controller.unit.model.delete.DeleteUnitMapper;
import br.com.benparvar.template.controller.unit.model.find.FindUnitMapper;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitMapper;
import br.com.benparvar.template.repository.unit.model.UnitModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit mapper config.
 */
@Configuration
public class UnitMapperConfig {

    /**
     * Gets unit model mapper.
     *
     * @return the unit model mapper
     */
    @Bean
    public UnitModelMapper getUnitModelMapper() {
        return UnitModelMapper.INSTANCE;
    }

    /**
     * Gets create unit mapper.
     *
     * @return the create unit mapper
     */
    @Bean
    public CreateUnitMapper getCreateUnitMapper() {
        return CreateUnitMapper.INSTANCE;
    }

    /**
     * Gets find unit mapper.
     *
     * @return the find unit mapper
     */
    @Bean
    public FindUnitMapper getFindUnitMapper() {
        return FindUnitMapper.INSTANCE;
    }

    /**
     * Gets update unit mapper.
     *
     * @return the update unit mapper
     */
    @Bean
    public UpdateUnitMapper getUpdateUnitMapper() {
        return UpdateUnitMapper.INSTANCE;
    }

    /**
     * Gets delete unit mapper.
     *
     * @return the delete unit mapper
     */
    @Bean
    public DeleteUnitMapper getDeleteUnitMapper() {
        return DeleteUnitMapper.INSTANCE;
    }

}
