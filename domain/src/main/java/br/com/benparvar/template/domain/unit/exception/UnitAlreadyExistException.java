package br.com.benparvar.template.domain.unit.exception;

import br.com.benparvar.template.domain.exception.AlreadyExistException;

public class UnitAlreadyExistException extends AlreadyExistException {
}
