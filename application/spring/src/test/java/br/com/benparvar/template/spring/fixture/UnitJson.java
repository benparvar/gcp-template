package br.com.benparvar.template.spring.fixture;

/**
 * The type Unit json.
 */
public class UnitJson {

    /**
     * The constant NO_PAYLOAD.
     */
    public static String NO_PAYLOAD = "";
    /**
     * The constant EMPTY_REQUEST.
     */
    public static String EMPTY_PAYLOAD = "{}";

    /**
     * The constant VALID_PAYLOAD.
     */
    public static String VALID_PAYLOAD = "{\"code\": \"MP\", \"name\": \"MILIPIECES\"}";

    /**
     * The constant VALID_UPDATE_PAYLOAD.
     */
    public static String VALID_UPDATE_PAYLOAD = "{\"code\": \"MP\", \"name\": \"MILI PIECES\"}";

    /**
     * The constant VALID_RESPONSE.
     */
    public static String VALID_RESPONSE = "{\"code\":\"MP\",\"name\":\"MILIPIECES\"}";

    /**
     * The constant VALID_UPDATE_RESPONSE.
     */
    public static String VALID_UPDATE_RESPONSE = "{\"code\":\"MP\",\"name\":\"MILI PIECES\"}";

    /**
     * The constant VALID_ARRAY_RESPONSE.
     */
    public static String VALID_ARRAY_RESPONSE = "[{\"code\":\"MP\",\"name\":\"MILIPIECES\"}]";
}
