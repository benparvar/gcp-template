package br.com.benparvar.template.domain.validator;

/**
 * The type String utils.
 */
public interface StringUtils {

    /**
     * Has text boolean.
     *
     * @param str the str
     * @return the boolean
     */
    public static boolean hasText(CharSequence str) {
        return (str != null && str.length() > 0 && containsText(str));
    }

    static boolean containsText(CharSequence str) {
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}
