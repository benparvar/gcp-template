package br.com.benparvar.template.controller.unit.model.delete;

import br.com.benparvar.template.domain.unit.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * The type Delete unit mapper.
 */
@Mapper
public interface DeleteUnitMapper {
    DeleteUnitMapper INSTANCE = Mappers.getMapper(DeleteUnitMapper.class);

    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    public Unit requestToEntity(DeleteUnitRequest request);
}
