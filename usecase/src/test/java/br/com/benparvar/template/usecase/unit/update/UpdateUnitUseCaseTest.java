package br.com.benparvar.template.usecase.unit.update;

import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.domain.unit.exception.UnitNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Update unit use case test.
 */
class UpdateUnitUseCaseTest {
    private  UpdateUnitUseCase uc;
    private final ValidateUnitBeforeUpdateUseCase validateUnitBeforeUpdateUseCase = mock(ValidateUnitBeforeUpdateUseCase.class);
    private final UpdateUnit updateUnitGateway = mock(UpdateUnit.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new UpdateUnitUseCase(validateUnitBeforeUpdateUseCase, updateUnitGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = null;

            when(validateUnitBeforeUpdateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("unit cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a entity already existent will sucess.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillSucess() {
        Unit entity = Unit.builder().code("PC").name("PIECE").build();
        when(validateUnitBeforeUpdateUseCase.execute(entity))
                .thenReturn(true);
        when(updateUnitGateway.update(entity)).thenReturn(entity);

        Unit result = uc.execute(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(validateUnitBeforeUpdateUseCase, times(1)).execute(entity);
        verify(updateUnitGateway, times(1)).update(entity);
    }

    /**
     * Execute with a entity non existent will fail.
     */
    @Test
    public void executeWithAEntityNonExistentWillFail() {
        assertThrows(UnitNotFoundException.class, () -> {
            Unit entity = Unit.builder().code("PC").name("PIECE").build();
            when(validateUnitBeforeUpdateUseCase.execute(entity))
                    .thenReturn(true);
            when(updateUnitGateway.update(entity)).thenThrow(new UnitNotFoundException());

            try {
                uc.execute(entity);
            } catch (UnitNotFoundException e) {
                verify(validateUnitBeforeUpdateUseCase, times(1)).execute(entity);
                verify(updateUnitGateway, times(1)).update(entity);

                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });

    }
}