package br.com.benparvar.template.spring.config.unit;

import br.com.benparvar.template.repository.unit.UnitRepository;
import br.com.benparvar.template.repository.unit.model.UnitModelMapper;
import br.com.benparvar.template.gateway.unit.UnitGateway;
import br.com.benparvar.template.usecase.unit.create.SaveUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit gateway config.
 */
@Configuration
public class UnitGatewayConfig {

    /**
     * Gets save unit.
     *
     * @param unitRepository    the unit repository
     * @param unitModelMapper the unit model mapper
     * @return the save unit
     */
    @Bean
    public SaveUnit getSaveUnit(UnitRepository unitRepository, UnitModelMapper unitModelMapper) {
        return new UnitGateway(unitRepository, unitModelMapper);
    }
}
