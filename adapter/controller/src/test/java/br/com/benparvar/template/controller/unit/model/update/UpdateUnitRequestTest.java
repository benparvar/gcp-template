package br.com.benparvar.template.controller.unit.model.update;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Update unit request test.
 */
class UpdateUnitRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateUnitRequest unit1 = new UpdateUnitRequest();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        UpdateUnitRequest unit2 = new UpdateUnitRequest();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateUnitRequest unit1 = new UpdateUnitRequest();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        UpdateUnitRequest unit2 = new UpdateUnitRequest();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateUnitRequest unit = new UpdateUnitRequest();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateUnitRequest unit = new UpdateUnitRequest();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("UpdateUnitRequest{code='LT', name='LITRE'}", unit.toString());
    }
}