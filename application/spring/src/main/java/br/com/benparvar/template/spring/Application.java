package br.com.benparvar.template.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author alans
 * The type Application.
 */
@Configuration
@EnableJpaRepositories("br.com.benparvar.template.database.*")
@ComponentScan(basePackages = {"br.com.benparvar.template.*"})
@EntityScan("br.com.benparvar.template.*")
@EnableAutoConfiguration
@SpringBootApplication
@EnableFeignClients
public class Application {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
