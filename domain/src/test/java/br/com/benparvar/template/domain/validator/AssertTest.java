package br.com.benparvar.template.domain.validator;

import br.com.benparvar.template.domain.validator.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Assert test.
 */
public class AssertTest {

    /**
     * Execute not null with a null object will fail.
     */
    @Test
    public void executeNotNullWithANullObjectWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                String object = null;
                Assert.notNull(object, "cannot be null");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute not null with a non null object will success.
     */
    @Test
    public void executeNotNullWithANonNullObjectWillSuccess() {
        assertDoesNotThrow(() -> {
            String object = "radio";
            Assert.notNull(object, "cannot be null");
        });
    }

    /**
     * Execute has text with a null string will fail.
     */
    @Test
    public void executeHasTextWithANullStringWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                String object = null;
                Assert.hasText(object, "cannot be null");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute has text with a empty string will fail.
     */
    @Test
    public void executeHasTextWithAEmptyStringWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                String object = "";
                Assert.hasText(object, "cannot be empty");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute has text with a string will sucess.
     */
    @Test
    public void executeHasTextWithAStringWillSucess() {
        assertDoesNotThrow(() -> {
            String object = "fail";
            Assert.hasText(object, "cannot be null");
        });
    }

    /**
     * Execute not empty with a null array will fail.
     */
    @Test
    public void executeNotEmptyWithANullArrayWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                String[] array = null;
                Assert.notEmpty(array, "cannot be null or empty");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute not empty with a empty array will fail.
     */
    @Test
    public void executeNotEmptyWithAEmptyArrayWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                String[] array = new String[0];
                Assert.notEmpty(array, "cannot be null or empty");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute not empty with a array will success.
     */
    @Test
    public void executeNotEmptyWithAArrayWillSuccess() {
        assertDoesNotThrow(() -> {
            String[] array = new String[]{"universe", "monster"};
            Assert.notEmpty(array, "cannot be null or empty");
        });
    }

    /**
     * Execute not empty with a null collection will fail.
     */
    @Test
    public void executeNotEmptyWithANullCollectionWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                List<String> list = null;
                Assert.notEmpty(list, "cannot be null or empty");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute not empty with a empty collection will fail.
     */
    @Test
    public void executeNotEmptyWithAEmptyCollectionWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                List<String> list = Collections.EMPTY_LIST;
                Assert.notEmpty(list, "cannot be null or empty");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute not empty with a collection will success.
     */
    @Test
    public void executeNotEmptyWithACollectionWillSuccess() {
        assertDoesNotThrow(() -> {
            List<String> list = Arrays.asList("Lily", "Robinho");
            Assert.notEmpty(list, "cannot be null or empty");
        });
    }

    /**
     * Execute is true with a false value will fail.
     */
    @Test
    public void executeIsTrueWithAFalseValueWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                boolean value = false;
                Assert.isTrue(value, "cannot be false");
            } catch (IllegalArgumentException e) {
                assertEquals("cannot be false", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute is true with a true value will success.
     */
    @Test
    public void executeIsTrueWithATrueValueWillSuccess() {
        assertDoesNotThrow(() -> {
            boolean value = true;
            Assert.isTrue(value, "cannot be false");
        });
    }

}