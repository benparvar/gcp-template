package br.com.benparvar.template.spring.config.unit;

import br.com.benparvar.template.repository.unit.UnitFirestoreRepository;
import br.com.benparvar.template.repository.unit.UnitRepository;
import com.google.cloud.firestore.Firestore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit repository config.
 */
@Configuration
public class UnitRepositoryConfig {

    /**
     * Gets unit repository.
     *
     * @param firestore the firestore
     * @return the unit repository
     */
    @Bean
    public UnitRepository getUnitRepository(Firestore firestore) {
        return new UnitFirestoreRepository(firestore);
    }
}
