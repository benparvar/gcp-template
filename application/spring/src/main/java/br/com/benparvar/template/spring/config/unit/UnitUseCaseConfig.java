package br.com.benparvar.template.spring.config.unit;

import br.com.benparvar.template.usecase.unit.create.CreateUnitUseCase;
import br.com.benparvar.template.usecase.unit.create.SaveUnit;
import br.com.benparvar.template.usecase.unit.create.ValidateUnitBeforeCreateUseCase;
import br.com.benparvar.template.usecase.unit.delete.DeleteUnit;
import br.com.benparvar.template.usecase.unit.delete.DeleteUnitUseCase;
import br.com.benparvar.template.usecase.unit.delete.ValidateUnitBeforeDeleteUseCase;
import br.com.benparvar.template.usecase.unit.find.all.FindAllUnits;
import br.com.benparvar.template.usecase.unit.find.all.FindAllUnitsUseCase;
import br.com.benparvar.template.usecase.unit.find.code.FindUnitByCode;
import br.com.benparvar.template.usecase.unit.find.code.FindUnitByCodeUseCase;
import br.com.benparvar.template.usecase.unit.update.UpdateUnit;
import br.com.benparvar.template.usecase.unit.update.UpdateUnitUseCase;
import br.com.benparvar.template.usecase.unit.update.ValidateUnitBeforeUpdateUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit use case config.
 */
@Configuration
public class UnitUseCaseConfig {

    /**
     * Gets validate unit before create use case.
     *
     * @return the validate unit before create use case
     */
    @Bean
    public ValidateUnitBeforeCreateUseCase getValidateUnitBeforeCreateUseCase() {
        return new ValidateUnitBeforeCreateUseCase();
    }

    /**
     * Gets create unit use case.
     *
     * @param validateUnitBeforeCreateUseCase the validate unit before create use case
     * @param saveUnitGateway                 the save unit gateway
     * @return the create unit use case
     */
    @Bean
    public CreateUnitUseCase getCreateUnitUseCase(ValidateUnitBeforeCreateUseCase validateUnitBeforeCreateUseCase,
                                                  SaveUnit saveUnitGateway) {
        return new CreateUnitUseCase(validateUnitBeforeCreateUseCase, saveUnitGateway);
    }

    /**
     * Gets find all units use case.
     *
     * @param findAllUnitsGateway the find all units gateway
     * @return the find all units use case
     */
    @Bean
    public FindAllUnitsUseCase getFindAllUnitsUseCase(FindAllUnits findAllUnitsGateway) {
        return new FindAllUnitsUseCase(findAllUnitsGateway);
    }

    /**
     * Gets find unit by code use case.
     *
     * @param findUnitByCodeGateway the find unit by code gateway
     * @return the find unit by code use case
     */
    @Bean
    public FindUnitByCodeUseCase getFindUnitByCodeUseCase(FindUnitByCode findUnitByCodeGateway) {
        return new FindUnitByCodeUseCase(findUnitByCodeGateway);
    }

    /**
     * Gets validate unit before update use case.
     *
     * @return the validate unit before update use case
     */
    @Bean
    public ValidateUnitBeforeUpdateUseCase getValidateUnitBeforeUpdateUseCase() {
        return new ValidateUnitBeforeUpdateUseCase();
    }

    /**
     * Gets update unit use case.
     *
     * @param validateUnitBeforeUpdateUseCase the validate unit before update use case
     * @param updateUnitGateway               the update unit gateway
     * @return the update unit use case
     */
    @Bean
    public UpdateUnitUseCase getUpdateUnitUseCase(ValidateUnitBeforeUpdateUseCase validateUnitBeforeUpdateUseCase,
                                                  UpdateUnit updateUnitGateway) {
        return new UpdateUnitUseCase(validateUnitBeforeUpdateUseCase, updateUnitGateway);
    }

    /**
     * Gets validate unit before delete use case.
     *
     * @return the validate unit before delete use case
     */
    @Bean
    public ValidateUnitBeforeDeleteUseCase getValidateUnitBeforeDeleteUseCase() {
        return new ValidateUnitBeforeDeleteUseCase();
    }

    /**
     * Gets delete unit use case.
     *
     * @param validateUnitBeforeDeleteUseCase the validate unit before delete use case
     * @param deleteUnitGateway               the delete unit gateway
     * @return the delete unit use case
     */
    @Bean
    public DeleteUnitUseCase getDeleteUnitUseCase(ValidateUnitBeforeDeleteUseCase validateUnitBeforeDeleteUseCase,
                                                  DeleteUnit deleteUnitGateway) {
        return new DeleteUnitUseCase(validateUnitBeforeDeleteUseCase, deleteUnitGateway);
    }
}
