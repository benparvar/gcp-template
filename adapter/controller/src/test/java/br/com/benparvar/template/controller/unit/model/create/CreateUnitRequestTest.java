package br.com.benparvar.template.controller.unit.model.create;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Create unit request test.
 */
class CreateUnitRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateUnitRequest unit1 = new CreateUnitRequest();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateUnitRequest unit2 = new CreateUnitRequest();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateUnitRequest unit1 = new CreateUnitRequest();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateUnitRequest unit2 = new CreateUnitRequest();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateUnitRequest unit = new CreateUnitRequest();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateUnitRequest unit = new CreateUnitRequest();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("CreateUnitRequest{code='LT', name='LITRE'}", unit.toString());
    }
}