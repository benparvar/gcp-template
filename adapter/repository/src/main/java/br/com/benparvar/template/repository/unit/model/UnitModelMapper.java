package br.com.benparvar.template.repository.unit.model;

import br.com.benparvar.template.domain.unit.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The type Unit mapper.
 */
@Mapper
public interface UnitModelMapper {

    UnitModelMapper INSTANCE = Mappers.getMapper(UnitModelMapper.class);

    /**
     * Model to entity unit.
     *
     * @param model the model
     * @return the unit
     */
    Unit modelToEntity(UnitModel model);

    /**
     * Model to entity list.
     *
     * @param models the models
     * @return the list
     */
    List<Unit> modelToEntity(List<UnitModel> models);

    /**
     * Entity to model unit model.
     *
     * @param entity the entity
     * @return the unit model
     */
    UnitModel entityToModel(Unit entity);
}
