package br.com.benparvar.template.usecase.unit.find.all;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The type Find all units use case test.
 */
class FindAllUnitsUseCaseTest {
    private final FindAllUnits findAllUnitsGateway = mock(FindAllUnits.class);
    private FindAllUnitsUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindAllUnitsUseCase(findAllUnitsGateway);
    }

    /**
     * Execute with no units in database will return an empty list.
     */
    @Test
    public void executeWithNoUnitsInDatabaseWillReturnAnEmptyList() {
        when(findAllUnitsGateway.findAll()).thenReturn(Collections.EMPTY_LIST);

        List<Unit> result = uc.execute();

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Execute with units in database will return an empty list.
     */
    @Test
    public void executeWithUnitsInDatabaseWillReturnAnEmptyList() {
        when(findAllUnitsGateway.findAll()).thenReturn(Arrays.asList(Unit.builder().code("PC").name("PIECES").build(),
                Unit.builder().code("MP").name("MILIPIECES").build()));

        List<Unit> result = uc.execute();

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
    }
}