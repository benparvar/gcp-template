package br.com.benparvar.template.controller.unit;

import br.com.benparvar.template.controller.unit.model.create.CreateUnitMapper;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitRequest;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitResponse;
import br.com.benparvar.template.controller.unit.model.delete.DeleteUnitMapper;
import br.com.benparvar.template.controller.unit.model.find.FindUnitMapper;
import br.com.benparvar.template.controller.unit.model.find.FindUnitResponse;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitMapper;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitRequest;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitResponse;
import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.usecase.unit.create.CreateUnitUseCase;
import br.com.benparvar.template.usecase.unit.delete.DeleteUnitUseCase;
import br.com.benparvar.template.usecase.unit.find.all.FindAllUnitsUseCase;
import br.com.benparvar.template.usecase.unit.find.code.FindUnitByCodeUseCase;
import br.com.benparvar.template.usecase.unit.update.UpdateUnitUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Unit controller test.
 */
class UnitControllerTest {
    private UnitController controller;
    private CreateUnitUseCase createUnitUseCase = mock(CreateUnitUseCase.class);
    private CreateUnitMapper createUnitMapper = CreateUnitMapper.INSTANCE;
    private FindAllUnitsUseCase findAllUnitsUseCase = mock(FindAllUnitsUseCase.class);
    private FindUnitMapper findUnitMapper = FindUnitMapper.INSTANCE;
    private FindUnitByCodeUseCase findUnitByCodeUseCase = mock(FindUnitByCodeUseCase.class);
    private UpdateUnitUseCase updateUnitUseCase = mock(UpdateUnitUseCase.class);
    private UpdateUnitMapper updateUnitMapper = UpdateUnitMapper.INSTANCE;
    private DeleteUnitUseCase deleteUnitUseCase = mock(DeleteUnitUseCase.class);
    private DeleteUnitMapper deleteUnitMapper = DeleteUnitMapper.INSTANCE;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        controller = new UnitController(createUnitUseCase, createUnitMapper, findAllUnitsUseCase, findUnitMapper,
                findUnitByCodeUseCase, updateUnitUseCase, updateUnitMapper, deleteUnitUseCase, deleteUnitMapper);
    }

    /**
     * Execute create use case with a null request will fail.
     */
    @Test
    public void executeCreateUseCaseWithANullRequestWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = null;
            CreateUnitRequest request = null;
            when(createUnitUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("unit cannot be null"));

            try {
                controller.create(request);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                verify(createUnitUseCase, times(1)).execute(entity);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute create use case with a valid request will success.
     */
    @Test
    public void executeCreateUseCaseWithAValidRequestWillSuccess() {
        Unit entity = Unit.builder().code("MP").name("Mili Pieces").build();
        CreateUnitRequest request = new CreateUnitRequest();
        request.setCode("MP");
        request.setName("Mili Pieces");
        when(createUnitUseCase.execute(entity)).thenReturn(entity);

        CreateUnitResponse response = controller.create(request);

        assertNotNull(response);
        assertEquals("MP", response.getCode());
        assertEquals("Mili Pieces", request.getName());
        verify(createUnitUseCase, times(1)).execute(entity);
    }

    /**
     * Execute find all use case with empty database will return a empty list.
     */
    @Test
    public void executeFindAllUseCaseWithEmptyDatabaseWillReturnAEmptyList() {
        when(findAllUnitsUseCase.execute()).thenReturn(Collections.EMPTY_LIST);

        List<FindUnitResponse> responses = controller.findAll();
        assertNotNull(responses);
        assertTrue(responses.isEmpty());
    }

    /**
     * Execute find all use case with database will return a list.
     */
    @Test
    public void executeFindAllUseCaseWithDatabaseWillReturnAList() {
        List<Unit> entities = Arrays.asList(Unit.builder().code("MP").name("Mili Pieces").build());

        when(findAllUnitsUseCase.execute()).thenReturn(entities);

        List<FindUnitResponse> responses = controller.findAll();
        assertNotNull(responses);
        assertFalse(responses.isEmpty());
        assertEquals(1, responses.size());
    }

    /**
     * Execute find by code with a valid code will success.
     */
    @Test
    public void executeFindByCodeWithAValidCodeWillSuccess() {
        String code = "MP";
        Unit entity = Unit.builder().code(code).name("Mili Pieces").build();

        when(findUnitByCodeUseCase.execute(code)).thenReturn(entity);

        FindUnitResponse response = controller.findByCode(code);
        assertNotNull(response);
        assertEquals("MP", response.getCode());
        assertEquals("Mili Pieces", response.getName());
    }

    /**
     * Execute update with an valid entity will success.
     */
    @Test
    public void executeUpdateWithAnValidEntityWillSuccess() {
        Unit entity = Unit.builder().code("MP").name("Milipieces").build();
        UpdateUnitRequest request = new UpdateUnitRequest();
        request.setCode("MP");
        request.setName("Milipieces");

        when(updateUnitUseCase.execute(entity)).thenReturn(entity);

        UpdateUnitResponse response = controller.update(request);
        assertEquals("MP", response.getCode());
        assertEquals("Milipieces", response.getName());
    }

    /**
     * Execute delete with an valid entity wil success.
     */
    @Test
    public void executeDeleteWithAnValidEntityWilSuccess() {
        assertDoesNotThrow(() -> {
            controller.delete("MT");
        });
    }
}