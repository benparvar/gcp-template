package br.com.benparvar.template.usecase.unit.delete;

import br.com.benparvar.template.domain.unit.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Delete unit use case.
 */
public class DeleteUnitUseCase {
    private final Logger log = LoggerFactory.getLogger(DeleteUnitUseCase.class);
    private final ValidateUnitBeforeDeleteUseCase validateUnitBeforeDeleteUseCase;
    private final DeleteUnit deleteUnitGateway;

    /**
     * Instantiates a new Delete unit use case.
     *
     * @param validateUnitBeforeDeleteUseCase the validate unit before delete use case
     * @param deleteUnitGateway               the delete unit gateway
     */
    public DeleteUnitUseCase(ValidateUnitBeforeDeleteUseCase validateUnitBeforeDeleteUseCase,
                             DeleteUnit deleteUnitGateway) {
        this.validateUnitBeforeDeleteUseCase = validateUnitBeforeDeleteUseCase;
        this.deleteUnitGateway = deleteUnitGateway;
    }

    /**
     * Execute.
     *
     * @param entity the entity
     */
    public void execute(Unit entity) {
        log.info("execute: {}", entity);

        validateUnitBeforeDeleteUseCase.execute(entity);
        deleteUnitGateway.delete(entity);
    }
}
