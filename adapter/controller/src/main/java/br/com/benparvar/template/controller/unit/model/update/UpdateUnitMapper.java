package br.com.benparvar.template.controller.unit.model.update;

import br.com.benparvar.template.domain.unit.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


/**
 * The type Update unit mapper.
 */
@Mapper
public interface UpdateUnitMapper {
    UpdateUnitMapper INSTANCE = Mappers.getMapper(UpdateUnitMapper.class);

    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    Unit requestToEntity(UpdateUnitRequest request);

    /**
     * Entity to response create unit response.
     *
     * @param entity the entity
     * @return the create unit response
     */
    UpdateUnitResponse entityToResponse(Unit entity);
}
