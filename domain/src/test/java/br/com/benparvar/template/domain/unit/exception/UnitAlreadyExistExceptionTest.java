package br.com.benparvar.template.domain.unit.exception;

import br.com.benparvar.template.domain.exception.AlreadyExistException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * The type Unit already exist exception test.
 */
class UnitAlreadyExistExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(AlreadyExistException.class, () -> {
            try {
                throw new UnitAlreadyExistException();
            } catch (AlreadyExistException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(UnitAlreadyExistException.class, () -> {
            try {
                throw new UnitAlreadyExistException();
            } catch (UnitAlreadyExistException e) {
                throw e;
            }
        });
    }
}