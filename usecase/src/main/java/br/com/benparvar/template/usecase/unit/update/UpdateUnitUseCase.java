package br.com.benparvar.template.usecase.unit.update;

import br.com.benparvar.template.domain.unit.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Update unit use case.
 */
public class UpdateUnitUseCase {
    private final Logger log = LoggerFactory.getLogger(UpdateUnitUseCase.class);
    private final ValidateUnitBeforeUpdateUseCase validateUnitBeforeUpdateUseCase;
    private final UpdateUnit updateUnitGateway;

    /**
     * Instantiates a new Update unit use case.
     *
     * @param validateUnitBeforeUpdateUseCase the validate unit before update use case
     * @param updateUnitGateway               the update unit gateway
     */
    public UpdateUnitUseCase(ValidateUnitBeforeUpdateUseCase validateUnitBeforeUpdateUseCase,
                             UpdateUnit updateUnitGateway) {
        this.validateUnitBeforeUpdateUseCase = validateUnitBeforeUpdateUseCase;
        this.updateUnitGateway = updateUnitGateway;
    }

    /**
     * Execute unit.
     *
     * @param entity the entity
     * @return the unit
     */
    public Unit execute(Unit entity) {
        log.info("execute: {}", entity);
        validateUnitBeforeUpdateUseCase.execute(entity);

        return updateUnitGateway.update(entity);
    }
}
