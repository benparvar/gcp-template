package br.com.benparvar.template.domain.unit.exception;

import br.com.benparvar.template.domain.exception.NotFoundException;

/**
 * The type Unit not found exception.
 */
public class UnitNotFoundException extends NotFoundException {
}
