package br.com.benparvar.template.controller.unit;

import br.com.benparvar.template.controller.unit.model.create.CreateUnitMapper;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitRequest;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitResponse;
import br.com.benparvar.template.controller.unit.model.delete.DeleteUnitMapper;
import br.com.benparvar.template.controller.unit.model.delete.DeleteUnitRequest;
import br.com.benparvar.template.controller.unit.model.find.FindUnitMapper;
import br.com.benparvar.template.controller.unit.model.find.FindUnitResponse;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitMapper;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitRequest;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitResponse;
import br.com.benparvar.template.usecase.unit.create.CreateUnitUseCase;
import br.com.benparvar.template.usecase.unit.delete.DeleteUnitUseCase;
import br.com.benparvar.template.usecase.unit.find.all.FindAllUnitsUseCase;
import br.com.benparvar.template.usecase.unit.find.code.FindUnitByCodeUseCase;
import br.com.benparvar.template.usecase.unit.update.UpdateUnitUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Unit controller.
 */
public class UnitController {
    private final Logger log = LoggerFactory.getLogger(UnitController.class);
    private final CreateUnitUseCase createUnitUseCase;
    private final CreateUnitMapper createUnitMapper;
    private final FindAllUnitsUseCase findAllUnitsUseCase;
    private final FindUnitMapper findUnitMapper;
    private final FindUnitByCodeUseCase findUnitByCodeUseCase;
    private final UpdateUnitUseCase updateUnitUseCase;
    private final UpdateUnitMapper updateUnitMapper;
    private final DeleteUnitUseCase deleteUnitUseCase;
    private final DeleteUnitMapper deleteUnitMapper;


    /**
     * Instantiates a new Unit controller.
     *
     * @param createUnitUseCase     the create unit use case
     * @param createUnitMapper      the create unit mapper
     * @param findAllUnitsUseCase   the find all units use case
     * @param findUnitMapper        the find unit mapper
     * @param findUnitByCodeUseCase the find unit by code use case
     * @param updateUnitUseCase     the update unit use case
     * @param updateUnitMapper      the update unit mapper
     * @param deleteUnitUseCase     the delete unit use case
     * @param deleteUnitMapper      the delete unit mapper
     */
    public UnitController(CreateUnitUseCase createUnitUseCase, CreateUnitMapper createUnitMapper,
                          FindAllUnitsUseCase findAllUnitsUseCase, FindUnitMapper findUnitMapper,
                          FindUnitByCodeUseCase findUnitByCodeUseCase, UpdateUnitUseCase updateUnitUseCase,
                          UpdateUnitMapper updateUnitMapper, DeleteUnitUseCase deleteUnitUseCase,
                          DeleteUnitMapper deleteUnitMapper) {
        this.createUnitUseCase = createUnitUseCase;
        this.createUnitMapper = createUnitMapper;
        this.findAllUnitsUseCase = findAllUnitsUseCase;
        this.findUnitMapper = findUnitMapper;
        this.findUnitByCodeUseCase = findUnitByCodeUseCase;
        this.updateUnitUseCase = updateUnitUseCase;
        this.updateUnitMapper = updateUnitMapper;
        this.deleteUnitUseCase = deleteUnitUseCase;
        this.deleteUnitMapper = deleteUnitMapper;
    }

    /**
     * Create create unit response.
     *
     * @param request the request
     * @return the create unit response
     */
    public CreateUnitResponse create(CreateUnitRequest request) {
        log.info("create: {}", request);

        return createUnitMapper.entityToResponse(createUnitUseCase.execute(createUnitMapper.requestToEntity(request)));
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    public List<FindUnitResponse> findAll() {
        log.info("findAll: ");

        return findUnitMapper.entityToResponse(findAllUnitsUseCase.execute());
    }

    /**
     * Find by code find unit response.
     *
     * @param code the code
     * @return the find unit response
     */
    public FindUnitResponse findByCode(String code) {
        log.info("findByCode: {}", code);

        return findUnitMapper.entityToResponse(findUnitByCodeUseCase.execute(code));
    }

    /**
     * Update.
     *
     * @param request the request
     * @return the update unit response
     */
    public UpdateUnitResponse update(UpdateUnitRequest request) {
        log.info("update: {}", request);

        return updateUnitMapper.entityToResponse(updateUnitUseCase.execute(updateUnitMapper.requestToEntity(request)));
    }

    /**
     * Delete.
     *
     * @param code the code
     */
    public void delete(String code) {
        log.info("delete: {}", code);

        deleteUnitUseCase.execute(deleteUnitMapper.requestToEntity(DeleteUnitRequest.newBuilder().code(code).build()));
    }
}
