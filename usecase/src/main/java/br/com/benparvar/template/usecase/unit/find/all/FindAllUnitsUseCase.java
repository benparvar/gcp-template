package br.com.benparvar.template.usecase.unit.find.all;

import br.com.benparvar.template.domain.unit.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Find all units use case.
 */
public class FindAllUnitsUseCase {
    private final Logger log = LoggerFactory.getLogger(FindAllUnitsUseCase.class);
    private final FindAllUnits findAllUnitsGateway;

    /**
     * Instantiates a new Find all units use case.
     *
     * @param findAllUnitsGateway the find all units gateway
     */
    public FindAllUnitsUseCase(FindAllUnits findAllUnitsGateway) {
        this.findAllUnitsGateway = findAllUnitsGateway;
    }

    /**
     * Execute list.
     *
     * @return the list
     */
    public List<Unit> execute() {
        log.info("execute:");
        return findAllUnitsGateway.findAll();
    }
}
