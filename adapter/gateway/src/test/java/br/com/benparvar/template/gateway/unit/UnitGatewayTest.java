package br.com.benparvar.template.gateway.unit;

import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.domain.unit.exception.UnitAlreadyExistException;
import br.com.benparvar.template.domain.unit.exception.UnitNotFoundException;
import br.com.benparvar.template.repository.unit.UnitRepository;
import br.com.benparvar.template.repository.unit.model.UnitModel;
import br.com.benparvar.template.repository.unit.model.UnitModelMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * The type Unit gateway test.
 */
class UnitGatewayTest {
    private final UnitRepository repository = mock(UnitRepository.class);
    private final UnitModelMapper mapper = UnitModelMapper.INSTANCE;
    private UnitGateway ugw;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        ugw = new UnitGateway(repository, mapper);
    }

    /**
     * Execute save with a entity already existent will fail.
     */
    @Test
    public void executeSaveWithAEntityAlreadyExistentWillFail() {
        assertThrows(UnitAlreadyExistException.class, () -> {
            Unit entity = Unit.builder().code("PC").name("PIECE").build();
            UnitModel model = mapper.entityToModel(entity);

            when(repository.findByCode(model.getCode())).thenThrow(new UnitAlreadyExistException());

            try {
                ugw.save(entity);
            } catch (UnitAlreadyExistException e) {
                verify(repository, times(1)).findByCode(model.getCode());
                verify(repository, times(0)).save(model);
                throw e;
            }

            fail("should throw an UnitAlreadyExistException");
        });
    }

    /**
     * Execute save with a entity non existent will success.
     */
    @Test
    public void executeSaveWithAEntityNonExistentWillSuccess() {
        Unit entity = Unit.builder().code("PC").name("PIECE").build();
        UnitModel model = mapper.entityToModel(entity);

        when(repository.findByCode(model.getCode())).thenReturn(null);
        when(repository.save(model)).thenReturn(model);

        Unit result = ugw.save(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(repository, times(1)).findByCode(model.getCode());
        verify(repository, times(1)).save(model);
    }

    /**
     * Execute save with a null entity will success.
     */
    @Test
    public void executeSaveWithANullEntityWillSuccess() {
        Unit entity = null;

        Unit result = ugw.save(entity);

        assertNull(result);

        verify(repository, times(0)).findByCode(any());
        verify(repository, times(0)).save(any());
    }

    /**
     * Execute find all without objects in database will fail.
     */
    @Test
    public void executeFindAllWithoutObjectsInDatabaseWillFail() {
        when(repository.findAll()).thenReturn(null);

        assertThrows(UnitNotFoundException.class, () -> {
            try {
                ugw.findAll();
            } catch (UnitNotFoundException e) {
                verify(repository, times(1)).findAll();
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute find all with objects in database will return a list.
     */
    @Test
    public void executeFindAllWithObjectsInDatabaseWillReturnAList() {
        UnitModel model = new UnitModel();
        model.setCode("PC");
        model.setName("PIECE");

        when(repository.findAll()).thenReturn(Arrays.asList(model));

        List<Unit> result = ugw.findAll();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals(model.getCode(), result.get(0).getCode());
        assertEquals(model.getName(), result.get(0).getName());
    }

    /**
     * Execute find by code without objects in database will fail.
     */
    @Test
    public void executeFindByCodeWithoutObjectsInDatabaseWillFail() {
        String code = "MT";
        when(repository.findByCode(code)).thenReturn(null);

        assertThrows(UnitNotFoundException.class, () -> {
            try {
                ugw.findByCode(code);
            } catch (UnitNotFoundException e) {
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute find by code with objects in database will return a list.
     */
    @Test
    public void executeFindByCodeWithObjectsInDatabaseWillReturnAList() {
        String code = "PC";
        UnitModel model = new UnitModel();
        model.setCode(code);
        model.setName("PIECE");

        when(repository.findByCode(code)).thenReturn(model);

        Unit result = ugw.findByCode(code);
        assertNotNull(result);
        assertEquals(model.getCode(), result.getCode());
        assertEquals(model.getName(), result.getName());
    }

    /**
     * Execute update with a non existent object will fail.
     */
    @Test
    public void executeUpdateWithANonExistentObjectWillFail() {
        String code = "AV";
        Unit entity = Unit.builder().code(code).name("Avoided").build();

        when(repository.findByCode(code)).thenReturn(null);

        assertThrows(UnitNotFoundException.class, () -> {
            try {
                ugw.update(entity);
            } catch (UnitNotFoundException e) {
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFound");
        });
    }

    /**
     * Execute update with a existent object will success.
     */
    @Test
    public void executeUpdateWithAExistentObjectWillSuccess() {
        String code = "AV";
        Unit entity = Unit.builder().code(code).name("Avoided").build();

        UnitModel oldModel = new UnitModel();
        oldModel.setCode(code);
        oldModel.setName("Soup");

        UnitModel newModel = new UnitModel();
        newModel.setCode(code);
        newModel.setName("Avoided");

        when(repository.findByCode(code)).thenReturn(oldModel);
        when(repository.save(newModel)).thenReturn(newModel);

        Unit result = ugw.update(entity);
        assertNotNull(result);
        assertEquals(newModel.getCode(), result.getCode());
        assertEquals(newModel.getName(), result.getName());

        verify(repository, times(1)).findByCode(code);
        verify(repository, times(1)).save(newModel);
    }

    /**
     * Execute delete with a non existent object will fail.
     */
    @Test
    public void executeDeleteWithANonExistentObjectWillFail() {
        String code = "AV";
        Unit entity = Unit.builder().code(code).name("Avoided").build();

        assertThrows(UnitNotFoundException.class, () -> {

            when(repository.findByCode(code)).thenThrow(new UnitNotFoundException());
            try {
                ugw.delete(entity);
            } catch (UnitNotFoundException e) {
                verify(repository, times(0)).save(any());
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFound");
        });
    }

    /**
     * Execute delete with a existent object will success.
     */
    @Test
    public void executeDeleteWithAExistentObjectWillSuccess() {
        String code = "AV";
        Unit entity = Unit.builder().code(code).name("Avoided").build();

        when(repository.findByCode(code)).thenReturn(mapper.entityToModel(entity));

        assertDoesNotThrow(() -> {
            ugw.delete(entity);
            verify(repository, times(1)).findByCode(code);
        });
    }
}