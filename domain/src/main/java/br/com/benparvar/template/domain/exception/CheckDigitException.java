package br.com.benparvar.template.domain.exception;

/**
 * The type Check digit exception.
 */
public class CheckDigitException extends RuntimeException {

    /**
     * Instantiates a new Check digit exception.
     */
    public CheckDigitException() {
        super();
    }

    /**
     * Instantiates a new Check digit exception.
     *
     * @param message the message
     */
    public CheckDigitException(String message) {
        super(message);
    }
}
