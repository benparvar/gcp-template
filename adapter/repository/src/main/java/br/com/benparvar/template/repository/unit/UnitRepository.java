package br.com.benparvar.template.repository.unit;

import br.com.benparvar.template.repository.unit.model.UnitModel;

import java.util.List;

/**
 * The interface Unit repository.
 */
public interface UnitRepository {
    /**
     * Find by code unit model.
     *
     * @param code the code
     * @return the unit model
     */
    UnitModel findByCode(String code);

    /**
     * Save unit model.
     *
     * @param entityToModel the entity to model
     * @return the unit model
     */
    UnitModel save(UnitModel entityToModel);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<UnitModel> findAll();

    /**
     * Delete.
     *
     * @param model the model
     */
    void delete(UnitModel model);
}
