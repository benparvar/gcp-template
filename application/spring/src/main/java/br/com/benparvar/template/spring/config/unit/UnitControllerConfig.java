package br.com.benparvar.template.spring.config.unit;


import br.com.benparvar.template.controller.unit.UnitController;
import br.com.benparvar.template.controller.unit.model.create.CreateUnitMapper;
import br.com.benparvar.template.controller.unit.model.delete.DeleteUnitMapper;
import br.com.benparvar.template.controller.unit.model.find.FindUnitMapper;
import br.com.benparvar.template.controller.unit.model.update.UpdateUnitMapper;
import br.com.benparvar.template.usecase.unit.create.CreateUnitUseCase;
import br.com.benparvar.template.usecase.unit.delete.DeleteUnitUseCase;
import br.com.benparvar.template.usecase.unit.find.all.FindAllUnitsUseCase;
import br.com.benparvar.template.usecase.unit.find.code.FindUnitByCodeUseCase;
import br.com.benparvar.template.usecase.unit.update.UpdateUnitUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit controller config.
 */
@Configuration
public class UnitControllerConfig {


    /**
     * Gets unit controller.
     *
     * @param createUnitUseCase     the create unit use case
     * @param createUnitMapper      the create unit mapper
     * @param findAllUnitsUseCase   the find all units use case
     * @param findUnitMapper        the find unit mapper
     * @param findUnitByCodeUseCase the find unit by code use case
     * @param updateUnitUseCase     the update unit use case
     * @param updateUnitMapper      the update unit mapper
     * @param deleteUnitUseCase     the delete unit use case
     * @param deleteUnitMapper      the delete unit mapper
     * @return the unit controller
     */
    @Bean
    public UnitController getUnitController(CreateUnitUseCase createUnitUseCase, CreateUnitMapper createUnitMapper,
                                            FindAllUnitsUseCase findAllUnitsUseCase, FindUnitMapper findUnitMapper,
                                            FindUnitByCodeUseCase findUnitByCodeUseCase, UpdateUnitUseCase updateUnitUseCase,
                                            UpdateUnitMapper updateUnitMapper, DeleteUnitUseCase deleteUnitUseCase,
                                            DeleteUnitMapper deleteUnitMapper) {
        return new UnitController(createUnitUseCase, createUnitMapper, findAllUnitsUseCase, findUnitMapper,
                findUnitByCodeUseCase, updateUnitUseCase, updateUnitMapper, deleteUnitUseCase, deleteUnitMapper);
    }
}
