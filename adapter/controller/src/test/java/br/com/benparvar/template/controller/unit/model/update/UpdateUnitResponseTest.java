package br.com.benparvar.template.controller.unit.model.update;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Update unit response test.
 */
class UpdateUnitResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateUnitResponse unit1 = new UpdateUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        UpdateUnitResponse unit2 = new UpdateUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateUnitResponse unit1 = new UpdateUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        UpdateUnitResponse unit2 = new UpdateUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");
        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateUnitResponse unit = new UpdateUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateUnitResponse unit = new UpdateUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("UpdateUnitResponse{code='LT', name='LITRE'}", unit.toString());
    }
}