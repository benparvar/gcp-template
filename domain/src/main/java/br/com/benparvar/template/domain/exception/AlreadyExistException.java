package br.com.benparvar.template.domain.exception;

/**
 * The type Product already exist exception.
 */
public class AlreadyExistException extends RuntimeException {
}
