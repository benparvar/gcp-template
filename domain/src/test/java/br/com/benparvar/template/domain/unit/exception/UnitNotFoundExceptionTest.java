package br.com.benparvar.template.domain.unit.exception;

import br.com.benparvar.template.domain.exception.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class UnitNotFoundExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new UnitNotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(UnitNotFoundException.class, () -> {
            try {
                throw new UnitNotFoundException();
            } catch (UnitNotFoundException e) {
                throw e;
            }
        });
    }
}