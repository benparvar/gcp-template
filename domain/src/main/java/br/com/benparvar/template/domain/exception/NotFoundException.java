package br.com.benparvar.template.domain.exception;

/**
 * The type Product not found exception.
 */
public class NotFoundException extends RuntimeException {
}
