package br.com.benparvar.template.usecase.unit.create;

import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.domain.unit.exception.UnitAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Create unit use case test.
 */
public class CreateUnitUseCaseTest {
    private CreateUnitUseCase uc;
    private final ValidateUnitBeforeCreateUseCase validateUnitBeforeCreateUseCase = mock(ValidateUnitBeforeCreateUseCase.class);
    private final SaveUnit saveUnitGateway = mock(SaveUnit.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new CreateUnitUseCase(validateUnitBeforeCreateUseCase, saveUnitGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = null;

            when(validateUnitBeforeCreateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("unit cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a entity already existent will fail.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillFail() {
        assertThrows(UnitAlreadyExistException.class, () -> {
            Unit entity = Unit.builder().code("PC").name("PIECE").build();
            when(validateUnitBeforeCreateUseCase.execute(entity))
                    .thenReturn(true);
            when(saveUnitGateway.save(entity)).thenThrow(new UnitAlreadyExistException());

            try {
                uc.execute(entity);
            } catch (UnitAlreadyExistException e) {
                verify(validateUnitBeforeCreateUseCase, times(1)).execute(entity);
                verify(saveUnitGateway, times(1)).save(entity);

                throw e;
            }

            fail("should throw an UnitAlreadyExistException");
        });
    }

    /**
     * Execute with a entity non existent will success.
     */
    @Test
    public void executeWithAEntityNonExistentWillSuccess() {
        Unit entity = Unit.builder().code("PC").name("PIECE").build();
        when(validateUnitBeforeCreateUseCase.execute(entity))
                .thenReturn(true);
        when(saveUnitGateway.save(entity)).thenReturn(entity);

        Unit result = uc.execute(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(validateUnitBeforeCreateUseCase, times(1)).execute(entity);
        verify(saveUnitGateway, times(1)).save(entity);
    }
}
