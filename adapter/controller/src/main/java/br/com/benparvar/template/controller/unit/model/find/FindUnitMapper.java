package br.com.benparvar.template.controller.unit.model.find;

import br.com.benparvar.template.domain.unit.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The type Find unit mapper.
 */
@Mapper
public interface FindUnitMapper {
    FindUnitMapper INSTANCE = Mappers.getMapper(FindUnitMapper.class);

    /**
     * Entity to response find unit response.
     *
     * @param entity the entity
     * @return the find unit response
     */
    FindUnitResponse entityToResponse(Unit entity);

    /**
     * Entity to response list.
     *
     * @param entities the entities
     * @return the list
     */
    List<FindUnitResponse> entityToResponse(List<Unit> entities);

}
