package br.com.benparvar.template.usecase.unit.find.code;

import br.com.benparvar.template.domain.unit.Unit;

/**
 * The interface Find unit by code.
 */
public interface FindUnitByCode {

    /**
     * Find by code unit.
     *
     * @param code the code
     * @return the unit
     */
    Unit findByCode(String code);
}
