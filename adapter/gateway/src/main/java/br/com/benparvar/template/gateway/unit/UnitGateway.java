package br.com.benparvar.template.gateway.unit;

import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.domain.unit.exception.UnitAlreadyExistException;
import br.com.benparvar.template.domain.unit.exception.UnitNotFoundException;
import br.com.benparvar.template.domain.validator.CollectionUtils;
import br.com.benparvar.template.repository.unit.UnitRepository;
import br.com.benparvar.template.repository.unit.model.UnitModel;
import br.com.benparvar.template.repository.unit.model.UnitModelMapper;
import br.com.benparvar.template.usecase.unit.create.SaveUnit;
import br.com.benparvar.template.usecase.unit.delete.DeleteUnit;
import br.com.benparvar.template.usecase.unit.find.all.FindAllUnits;
import br.com.benparvar.template.usecase.unit.find.code.FindUnitByCode;
import br.com.benparvar.template.usecase.unit.update.UpdateUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Unit gateway.
 */
public class UnitGateway implements SaveUnit, FindAllUnits, FindUnitByCode, UpdateUnit, DeleteUnit {
    private final Logger log = LoggerFactory.getLogger(UnitGateway.class);
    private final UnitRepository repository;
    private final UnitModelMapper mapper;

    /**
     * Instantiates a new Unit gateway.
     *
     * @param repository        the repository
     * @param mapper
     */
    public UnitGateway(UnitRepository repository, UnitModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Unit save(Unit entity) {
        log.info("saving: {}", entity);

        if (null == entity) {
            return null;
        }

        if (null != repository.findByCode(entity.getCode()))
            throw new UnitAlreadyExistException();

        UnitModel model = repository.save(UnitModelMapper.INSTANCE.entityToModel(entity));
        log.info("saved: {}", model);

        return mapper.modelToEntity(model);
    }

    @Override
    public List<Unit> findAll() {
        log.info("finding All:");

        List<UnitModel> models = repository.findAll();
        if (CollectionUtils.isEmpty(models))
            throw new UnitNotFoundException();

        return mapper.modelToEntity(models);
    }

    @Override
    public Unit findByCode(String code) {
        log.info("finding by code: {}", code);

        UnitModel model = repository.findByCode(code);
        if (null == model)
            throw new UnitNotFoundException();

        return mapper.modelToEntity(model);
    }

    @Override
    public void delete(Unit entity) {
        log.info("deleting: {}", entity);

        UnitModel model = repository.findByCode(entity.getCode());
        if (null == model)
            throw new UnitNotFoundException();

        repository.delete(model);
    }

    @Override
    public Unit update(Unit entity) {
        log.info("updating: {}", entity);

        if (null == entity)
            return null;

        UnitModel model = repository.findByCode(entity.getCode());
        if (null == model)
            throw new UnitNotFoundException();

        model.setName(entity.getName());

        UnitModel updatedModel = repository.save(model);
        log.info("updated: {}", updatedModel);

        return mapper.modelToEntity(updatedModel);
    }
}
