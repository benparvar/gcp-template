package br.com.benparvar.template.controller.unit.model.update;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update unit mapper test.
 */
class UpdateUnitMapperTest {
    private UpdateUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = UpdateUnitMapper.INSTANCE;
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Unit entity = null;
        UpdateUnitResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Unit entity = Unit.builder().code("MT").name("Meter").build();

        UpdateUnitResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        UpdateUnitRequest request = null;
        Unit unit = mapper.requestToEntity(request);

        assertNull(unit, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        UpdateUnitRequest request = new UpdateUnitRequest();
        request.setCode("MT");
        request.setName("Meter");

        Unit unit = mapper.requestToEntity(request);

        assertNotNull(unit, "do not must be null");
        assertEquals("MT", unit.getCode());
        assertEquals("Meter", unit.getName());
    }
}