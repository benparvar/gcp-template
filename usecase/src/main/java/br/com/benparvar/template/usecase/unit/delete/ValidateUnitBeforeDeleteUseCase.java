package br.com.benparvar.template.usecase.unit.delete;

import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.domain.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate unit before delete use case.
 */
public class ValidateUnitBeforeDeleteUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateUnitBeforeDeleteUseCase.class);

    /**
     * Execute boolean.
     *
     * @param entity the entity
     * @return the boolean
     */
    public boolean execute(Unit entity) {
        log.info("execute: {}", entity);
        Assert.notNull(entity, "unit cannot be null");
        Assert.hasText(entity.getCode(), "unit code must have a text");

        return true;
    }
}
