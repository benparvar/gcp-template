package br.com.benparvar.template.domain.unit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Unit test.
 */
class UnitTest {

    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        Unit unit1 = Unit.builder().code("LT").name("LITRE").build();
        Unit unit2 = Unit.builder().code("LT").name("LITRE").build();

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        Unit unit1 = Unit.builder().code("LT").name("LITRE").build();
        Unit unit2 = Unit.builder().code("LT").name("LITRE").build();

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        Unit unit = Unit.builder().code("LT").name("LITRE").build();

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        Unit unit = Unit.builder().code("LT").name("LITRE").build();

        assertNotNull(unit);
        assertEquals("Unit(code=LT, name=LITRE)", unit.toString());
    }
}