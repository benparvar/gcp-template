package br.com.benparvar.template.controller.unit.model.create;

import br.com.benparvar.template.domain.unit.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * The type Create unit mapper.
 */
@Mapper
public interface CreateUnitMapper {
    CreateUnitMapper INSTANCE = Mappers.getMapper(CreateUnitMapper.class);

    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    Unit requestToEntity(CreateUnitRequest request);

    /**
     * Entity to response create unit response.
     *
     * @param entity the entity
     * @return the create unit response
     */
    CreateUnitResponse entityToResponse(Unit entity);
}
