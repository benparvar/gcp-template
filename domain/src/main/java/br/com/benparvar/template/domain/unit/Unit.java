package br.com.benparvar.template.domain.unit;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Unit.
 */
@Data
@Builder
public class Unit implements Serializable {
    private String code;
    private String name;
}
