package br.com.benparvar.template.usecase.unit.update;

import br.com.benparvar.template.domain.unit.Unit;
import br.com.benparvar.template.domain.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate unit before update use case.
 */
public class ValidateUnitBeforeUpdateUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateUnitBeforeUpdateUseCase.class);

    /**
     * Execute boolean.
     *
     * @param entity the entity
     * @return the boolean
     */
    public boolean execute(Unit entity) {
        log.info("execute: {}", entity);
        Assert.notNull(entity, "unit cannot be null");
        Assert.hasText(entity.getCode(), "unit code must have a text");
        Assert.hasText(entity.getName(), "unit name must have a text");

        return true;
    }
}
