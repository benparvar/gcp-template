package br.com.benparvar.template.domain.validator;

import br.com.benparvar.template.domain.validator.CollectionUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The type Collection utils test.
 */
public class CollectionUtilsTest {
    /**
     * Execute is empty with a null collection will return true.
     */
    @Test
    public void executeIsEmptyWithANullCollectionWillReturnTrue() {
        List<String> collection = null;

        boolean result = CollectionUtils.isEmpty(collection);
        assertTrue(result);
    }

    /**
     * Execute is empty with a empty collection will return true.
     */
    @Test
    public void executeIsEmptyWithAEmptyCollectionWillReturnTrue() {
        List<String> collection = new ArrayList<>();

        boolean result = CollectionUtils.isEmpty(collection);
        assertTrue(result);
    }

    /**
     * Execute is empty with a non empty collection will return false.
     */
    @Test
    public void executeIsEmptyWithANonEmptyCollectionWillReturnFalse() {
        List<String> collection = Arrays.asList("Compulsion", "I had this thing");

        boolean result = CollectionUtils.isEmpty(collection);
        assertFalse(result);
    }

    /**
     * Execute is not empty with a null collection will return false.
     */
    @Test
    public void executeIsNotEmptyWithANullCollectionWillReturnFalse() {
        List<String> collection = null;

        boolean result = CollectionUtils.isNotEmpty(collection);
        assertFalse(result);
    }

    /**
     * Execute is not empty with a empty collection will return false.
     */
    @Test
    public void executeIsNotEmptyWithAEmptyCollectionWillReturnFalse() {
        List<String> collection = new ArrayList<>();

        boolean result = CollectionUtils.isNotEmpty(collection);
        assertFalse(result);
    }

    /**
     * Execute is not empty with a non empty collection will return true.
     */
    @Test
    public void executeIsNotEmptyWithANonEmptyCollectionWillReturnTrue() {
        List<String> collection = Arrays.asList("Compulsion", "I had this thing");

        boolean result = CollectionUtils.isNotEmpty(collection);
        assertTrue(result);
    }
}