package br.com.benparvar.template.usecase.unit.find.all;

import br.com.benparvar.template.domain.unit.Unit;

import java.util.List;

/**
 * The interface Find all units.
 */
public interface FindAllUnits {
    /**
     * Find all list.
     *
     * @return the list
     */
    List<Unit> findAll();
}
