package br.com.benparvar.template.controller.unit.model.delete;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Delete unit mapper test.
 */
class DeleteUnitMapperTest {
    private DeleteUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = DeleteUnitMapper.INSTANCE;
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        DeleteUnitRequest request = null;
        Unit unit = mapper.requestToEntity(request);

        assertNull(unit, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        DeleteUnitRequest request = DeleteUnitRequest.newBuilder().code("MT").name("Meter").build();

        Unit unit = mapper.requestToEntity(request);

        assertNotNull(unit, "do not must be null");
        assertEquals("MT", unit.getCode());
        assertEquals("Meter", unit.getName());
    }
}