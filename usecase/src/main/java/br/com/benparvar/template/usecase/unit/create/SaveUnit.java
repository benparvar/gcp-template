package br.com.benparvar.template.usecase.unit.create;

import br.com.benparvar.template.domain.unit.Unit;

/**
 * The interface Save unit.
 */
public interface SaveUnit {

    /**
     * Save unit.
     *
     * @param entity the entity
     * @return the unit
     */
    Unit save(Unit entity);
}
