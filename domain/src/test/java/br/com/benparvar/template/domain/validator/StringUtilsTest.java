package br.com.benparvar.template.domain.validator;

import br.com.benparvar.template.domain.validator.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The type String utils test.
 */
public class StringUtilsTest {

    /**
     * Execute has text with null text will return false.
     */
    @Test
    public void executeHasTextWithNullTextWillReturnFalse() {
        String text = null;

        boolean result = StringUtils.hasText(text);
        assertFalse(result);
    }

    /**
     * Execute has text with empty text will return false.
     */
    @Test
    public void executeHasTextWithEmptyTextWillReturnFalse() {
        String text = "";

        boolean result = StringUtils.hasText(text);
        assertFalse(result);
    }

    /**
     * Execute has text with empty spaces text will return false.
     */
    @Test
    public void executeHasTextWithEmptySpacesTextWillReturnFalse() {
        String text = "  ";

        boolean result = StringUtils.hasText(text);
        assertFalse(result);
    }

    /**
     * Execute has text with text will return true.
     */
    @Test
    public void executeHasTextWithTextWillReturnTrue() {
        String text = " r  ";

        boolean result = StringUtils.hasText(text);
        assertTrue(result);
    }
}
