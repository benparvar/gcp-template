package br.com.benparvar.template.usecase.unit.delete;

import br.com.benparvar.template.domain.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Delete unit use case test.
 */
class DeleteUnitUseCaseTest {
    private DeleteUnitUseCase uc;
    private final ValidateUnitBeforeDeleteUseCase validateUnitBeforeDeleteUseCase = mock(ValidateUnitBeforeDeleteUseCase.class);
    private final DeleteUnit deleteUnitGateway = mock(DeleteUnit.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new DeleteUnitUseCase(validateUnitBeforeDeleteUseCase, deleteUnitGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = null;

            when(validateUnitBeforeDeleteUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("unit cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }


    /**
     * Execute with a entity already existent will success.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillSuccess() {
        Unit entity = Unit.builder().code("PC").name("PIECE").build();
        when(validateUnitBeforeDeleteUseCase.execute(entity))
                .thenReturn(true);

        assertDoesNotThrow(() -> {
            uc.execute(entity);

        });

        verify(validateUnitBeforeDeleteUseCase, times(1)).execute(entity);
        verify(deleteUnitGateway, times(1)).delete(entity);
    }


    /**
     * Execute with a entity non existent will fail.
     */
    @Test
    public void executeWithAEntityNonExistentWillFail() {
        // TODO -- Implement and catch UnitNotFoundException
    }
}